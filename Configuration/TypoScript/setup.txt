# Plugin settings
plugin.tx_pxaleadgenerator {
	view {
		templateRootPath = {$plugin.tx_pxaleadgenerator.view.templateRootPath}
		layoutRootPath = {$plugin.tx_pxaleadgenerator.view.layoutRootPath}
	}
	persistence {
		storagePid = {$plugin.tx_pxaleadgenerator.persistence.storagePid}
	}
}

# Add jquery validate
# page.includeJSFooterlibs.jquery_validate = EXT:pxa_lead_generator/Resources/Public/js/jquery.validate.min.js
# page.includeJSFooterlibs.jqv_messages_sv = EXT:pxa_lead_generator/Resources/Public/js/messages_sv.js

# Add this to TS setup if you don't have bootstrap modal included
page.includeJSFooter.bootstrap_modal = EXT:pxa_lead_generator/Resources/Public/js/bootstrap/modal.js
page.includeJSFooter.pxa_lead_generator_js = EXT:pxa_lead_generator/Resources/Public/js/pxa_lead_generator.js

# Css
# Add this to TS setup if you don't have bootstrap css included
# page.includeCSS.bootstrapCss = EXT:pxa_lead_generator/Resources/Public/css/bootstrap.min.css

# Add plugin to all pages
page.171717 = RECORDS
page.171717 {
  tables = tt_content
  source = {$plugin.tx_pxaleadgenerator.settings.lead_generator_uid}
}

leadGeneratorAjax = PAGE
leadGeneratorAjax.typeNum = 127498532
leadGeneratorAjax.config.no_cache = 1
leadGeneratorAjax.config.disableAllHeaderCode = 1

leadGeneratorAjax.5 < page.171717

# Default styles

plugin.tx_pxaleadgenerator._CSS_DEFAULT_STYLE (
	textarea.f3-form-error {
		background-color:#FF9F9F;
		border: 1px #FF0000 solid;
	}

	input.f3-form-error {
		background-color:#FF9F9F;
		border: 1px #FF0000 solid;
	}

	.tx-pxa-lead-generator table {
		border-collapse:separate;
		border-spacing:10px;
	}

	.tx-pxa-lead-generator table th {
		font-weight:bold;
	}

	.tx-pxa-lead-generator table td {
		vertical-align:top;
	}

	.typo3-messages .message-error {
		color:red;
	}

	.typo3-messages .message-ok {
		color:green;
	}

	#pxa_lead_generator .section-1 {
		border-right: 1px solid grey;
		 margin: 10px 0;
	}

	#pxa_lead_generator .section-2 {
		margin: 10px 0;
	}

	#pxa_lead_generator h3 {
		text-align: center;
	}

	#pxa_lead_generator .text1, #pxa_lead_generator .text2 {
		text-align: center;
		padding:20px;
	}

	#pxa_lead_generator .lg-form label {
		width: 20%;
	}

	#pxa_lead_generator .lg-form input[type="text"] {
		width: 70%;
	}

	#pxa_lead_generator .sticky-btn {
		color: white;
		position: fixed;
		width: 180px;
		height: 60px;
		top: 60%;
		z-index: 2000;
		background: #66bc29;
	}

	#pxa_lead_generator .sticky-btn:hover {
		background: #8FD95B;
	}

	#pxa_lead_generator .sticky-button-right {
		right: -60px;

		-webkit-border-top-left-radius: 10px;
		-webkit-border-top-right-radius: 10px;
		-moz-border-radius-topleft: 10px;
		-moz-border-radius-topright: 10px;
		border-top-left-radius: 10px;
		border-top-right-radius: 10px;

		-webkit-border-bottom-right-radius: 0px;
		-webkit-border-bottom-left-radius: 0px;
		-moz-border-radius-bottomright: 0px;
		-moz-border-radius-bottomleft: 0px;
		border-bottom-right-radius: 0px;
		border-bottom-left-radius: 0px;
	}

	#pxa_lead_generator .sticky-button-left {
		left: -60px;

		-webkit-border-top-left-radius: 0px;
		-webkit-border-top-right-radius: 0px;
		-moz-border-radius-topleft: 0px;
		-moz-border-radius-topright: 0px;
		border-top-left-radius: 0px;
		border-top-right-radius: 0px;

		-webkit-border-bottom-right-radius: 10px;
		-webkit-border-bottom-left-radius: 10px;
		-moz-border-radius-bottomright: 10px;
		-moz-border-radius-bottomleft: 10px;
		border-bottom-right-radius: 10px;
		border-bottom-left-radius: 10px;
	}

	.rotate {

		/* Safari */
		-webkit-transform: rotate(-90deg);

		/* Firefox */
		-moz-transform: rotate(-90deg);

		/* IE */
		-ms-transform: rotate(-90deg);

		/* Opera */
		-o-transform: rotate(-90deg);

		/* Internet Explorer */
		/*filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);*/

		-sand-transform: rotate(-90deg);
	}

	#pxa_lead_generator .lg-form label.error {
		color:red;
		width:100%;
	}

	.pxaleadgen-ovfl-hidden {
		overflow: hidden;
	}

	.modal-backdrop.fade.in {
		display: none !important;
	}
)
