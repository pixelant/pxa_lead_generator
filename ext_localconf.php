<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Pixelant.PxaLeadGenerator',
	'Pxaleadgenerator',
	array(
		'LeadGenerator' => 'show,sendmail,setActive,createEmail',
		
	),
	// non-cacheable actions
	array(
		'LeadGenerator' => '',
	)
);
