<?php
namespace Pixelant\PxaLeadGenerator\Tests\Unit\Controller;
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Pavlo Zaporozjkyi <pavlo@pixelant.se>, Pixelant
 *  			
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class Pixelant\PxaLeadGenerator\Controller\LeadGeneratorController.
 *
 * @author Pavlo Zaporozjkyi <pavlo@pixelant.se>
 */
class LeadGeneratorControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase {

	/**
	 * @var \Pixelant\PxaLeadGenerator\Controller\LeadGeneratorController
	 */
	protected $subject = NULL;

	protected function setUp() {
		$this->subject = $this->getMock('Pixelant\\PxaLeadGenerator\\Controller\\LeadGeneratorController', array('redirect', 'forward', 'addFlashMessage'), array(), '', FALSE);
	}

	protected function tearDown() {
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function showActionAssignsTheGivenLeadGeneratorToView() {
		$leadGenerator = new \Pixelant\PxaLeadGenerator\Domain\Model\LeadGenerator();

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$this->inject($this->subject, 'view', $view);
		$view->expects($this->once())->method('assign')->with('leadGenerator', $leadGenerator);

		$this->subject->showAction($leadGenerator);
	}
}
