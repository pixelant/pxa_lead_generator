# Pixelant

## Lead Generator (pxa_lead_generator)

This is an extension that is used for fetch any configured set of content element(s). Which
is loaded into a Modal for display on any TYPO3 webpage. It also contains a trigger button
for interaction in different shapes.