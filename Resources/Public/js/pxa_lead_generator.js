 $(function() {

    if( $('.tx-pxa-lead-generator').length) {

    var interval = $("#pxa_lead_generator .modal-content").attr("data-interval");
    var pagesToShow = $("#pxa_lead_generator .modal-content").attr("data-show");
    var disablePopup = $("#pxa_lead_generator .modal-content").attr("data-disablepopup");
    var timer = 0;
    var deviceWidth = (window.innerWidth > 0) ? window.innerWidth : screen.width;

    if (interval != -1 && disablePopup != 1) {
        timer = setTimeout( function() {
                showLGModal();
                $.ajax({
                    async: 'true',
                    url: '?type=127498532&tx_pxaleadgenerator_pxaleadgenerator%5BisActive%5D=1&tx_pxaleadgenerator_pxaleadgenerator%5Baction%5D=setActive&tx_pxaleadgenerator_pxaleadgenerator%5Bcontroller%5D=LeadGenerator',
                    type: 'POST',
                    dataType: 'JSON',
                    success: function(data) {
                    }
                });
            }, interval * 1000);
    }

    $('#myModal').bind('hidden.bs.modal', function () {
        $("html").removeClass("pxaleadgen-ovfl-hidden");
    });

    $('#myModal').bind('show.bs.modal', function () {
        $("html").addClass("pxaleadgen-ovfl-hidden");
    });

    $("#pxa_lead_generator .show-modal").click( function(e){

        // Tell php session that user already has activated the modal

        $.ajax({
            async: 'true',
            url: '?type=127498532&tx_pxaleadgenerator_pxaleadgenerator%5BisActive%5D=1&tx_pxaleadgenerator_pxaleadgenerator%5Baction%5D=setActive&tx_pxaleadgenerator_pxaleadgenerator%5Bcontroller%5D=LeadGenerator',
            type: 'POST',
            dataType: 'html',
            success: function(data) {
            }
        });

        // Reset the timer, so modal wont popup twice

        if( timer ) {
            clearTimeout(timer);
        }

    });

    function showLGModal() {
        if(deviceWidth > 768){
            $('#pxa_lead_generator #myModal').modal();
        }
    }

    // Jquery validation

//  $("#pxa_lead_generator .lg-form").validate();
//
//  $( "#pxa_lead_generator .lg-form #email" ).rules( "add", {
//    required: true,
//    email: true,
//  });
//
    }

    $(document).ready (function (){
        if ( $("div.hidden").find("div.tx-powermail").length > 0 && $("#pxa-lead-generator-for-powermail").length > 0){
        $("div.hidden").find("div.tx-powermail").appendTo("#pxa-lead-generator-for-powermail");
        $(".tx-pxa-lead-generator").appendTo("body");
        }

        if( $("#pxa_lead_generator .modal-content").attr("data-show-modal") == 1 && disablePopup != 1 ) {
        setTimeout (showLGModal, 2000);
        }
    });

});



