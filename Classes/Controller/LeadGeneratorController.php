<?php
namespace Pixelant\PxaLeadGenerator\Controller;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014 Pavlo Zaporozjkyi <pavlo@pixelant.se>, Pixelant
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use \TYPO3\CMS\Core\Utility\GeneralUtility;
use \TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;

/**
 * LeadGeneratorController
 */
class LeadGeneratorController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	/**
	 * action show
	 *
	 * @return void
	 */
	public function showAction() {

		$showModal = 0;
		$sessionData = $GLOBALS['TSFE']->fe_user->getKey('ses', 'pxaleadgenerator');

		// Set default session values
		if (!isset($sessionData['was_modal_active']) ) {
			$sessionData['was_modal_active'] = 0;
			$sessionData['time'] = 0;
			$sessionData['pages_passed'] = 0;
		}

		// Set session variables
		if ($sessionData['was_modal_active'] == 0) {

			if( $sessionData['time'] == 0 ) {
				$sessionData['time'] = $GLOBALS['SIM_EXEC_TIME'] + $this->settings['time_before_show'];
			}

			$sessionData['pages_passed']++;

			if( $sessionData['pages_passed'] >= $this->settings['pages_before_show'] ) {
				$showModal = 1;
				$sessionData['was_modal_active'] = 1;
				$sessionData['pages_passed'] = 0;
				$sessionData['time'] = 0;
			}

		}

		// Calculate interval
		if ($sessionData['time'] != 0 ) {
			$interval = $sessionData['time'] - $GLOBALS['SIM_EXEC_TIME'];
			$interval = ($interval < 0) ? 0 : $interval;
		} else {
			$interval = -1;
		}

		// Store session
		$GLOBALS['TSFE']->fe_user->setKey('ses', 'pxaleadgenerator', $sessionData);
		$GLOBALS['TSFE']->fe_user->storeSessionData();

		// Get body content
		$cObj = GeneralUtility::makeInstance(ContentObjectRenderer::class);

		$ttContentConfig = array(
			'tables' => 'tt_content',
			'source' => $this->settings['bodyCEUid'],
			'dontCheckPid' => 1
		);

		$content = $cObj->cObjGetSingle('RECORDS', $ttContentConfig);

		// Assigns
		$this->view->assignMultiple([
			'content' => $content,
			'showModal' => $showModal,
			'interval' => $interval
		]);
	}

	/**
	 * action setActive
	 *
	 * @return void
	 */
	public function setActiveAction() {

		// Get session
		$sessionData = $GLOBALS['TSFE']->fe_user->getKey('ses', 'pxaleadgenerator');

		// Change was activated to true and reset time and pages counters
		$sessionData['was_modal_active'] = 1;
		$sessionData['time'] = 0;
		$sessionData['pages_passed'] = 0;

		// Store session

		$GLOBALS['TSFE']->fe_user->setKey('ses', 'pxaleadgenerator', $sessionData);
		$GLOBALS['TSFE']->fe_user->storeSessionData();

		return 1;
	}

	/**
	 * action sendmail
	 *
	 * @return void
	 */
	public function sendmailAction() {

		// Get arguments
		$args = $this->request->getArguments();

		if (isset($args['pxa_lg_formdata'])) {
			$formData = $args['pxa_lg_formdata'];
		}

		// Set variables
		$senderName = isset($formData['name']) ? $formData['name'] : 'Guest';
		$senderEmail = isset($formData['email']) ? $formData['email'] : '';

		$recieverName = $this->settings['reciever_name'];
		$recieverEmail = $this->settings['reciever_email'];

		$city = isset($formData['city']) ? $formData['city'] : '';
		$subject = !empty( $this->settings['admin_email_subject'] ) ? $this->settings['admin_email_subject'] : 'Lead generator';

		// Create TYPO3 mail message instance
		$message = GeneralUtility::makeInstance('t3lib_mail_Message');

		// Send mail if sender and reciever is set

		if (!empty($senderEmail) && !empty($recieverEmail)) {

			$recipient = array($recieverEmail => $recieverName);
			$sender = array($senderEmail => $senderName);

			$message->setTo($recipient)
			->setFrom($sender)
			->setSubject($subject);

			// Populate email body with the values from form data
			$this->populateEmailBody($formData);
			$message->setBody($this->settings['admin_mail_body'], 'text/plain');

			// Send
			$message->send();
		}

		$this->forward("show");

	}

	/**
	 * Goes through the admin_mail_body and replaces special markers {@marker} with the values from the given formData array
	 */
	public function populateEmailBody($formData) {
		if (is_array($formData) ) {
			foreach ($formData as $key => $value) {
				$pattern = '/\{\@' . preg_quote($key, '/') . '\}/';
				$this->settings['admin_mail_body'] = preg_replace($pattern, $value, $this->settings['admin_mail_body']);
			}
		}
	}

}
